class timezone(
  $zone = 'US/Pacific'
){

  file { '/etc/timezone':
    content => "${zone}\n"
  }

  file { '/etc/localtime':
    ensure => present,
    source => "/usr/share/zoneinfo/${zone}",
    links  => follow,
    force  => true,
  }
}
