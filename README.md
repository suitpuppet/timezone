Set the timezone. Currently only tested on Debian.

Use:

    include timezone

Use `US/Pacific` as the default timezone. To override, set
the class parameter `timezone:zone`.
